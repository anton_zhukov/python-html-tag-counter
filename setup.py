from setuptools import setup

setup(
    name='tagcounter',
    version='1.0',
    author='Anton Zhukau',
    packages=['tagcounter'],
    description='HTML tag counter',
    package_data={'': ['*.yaml']},
    entry_points={'console_scripts': ['tagcounter = tagcounter.counter:main']}, install_requires=['lxml', 'PyYAML',
                                                                                                  'sqlalchemy']
)
