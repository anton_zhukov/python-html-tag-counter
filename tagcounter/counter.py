#!/usr/bin/env python3
# encoding: utf-8

import argparse
import json
import logging
import urllib.request
from collections import Counter
from datetime import datetime
from tkinter import *
from urllib.parse import urlparse

import lxml.html
import yaml
from sqlalchemy import Column, Integer, String, DateTime, JSON
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

Base = declarative_base()


class WebPageData(Base):
    __tablename__ = 'WebPageData'
    id = Column(Integer, primary_key=True)
    host_name = Column(String(250), nullable=False)
    url = Column(String(250), nullable=False)
    parsing_data = Column(JSON, nullable=False)
    check_date = Column(DateTime, nullable=False)


engine = create_engine('sqlite:///counter.db')
Base.metadata.create_all(engine)
DBSession = sessionmaker(bind=engine)
session = DBSession()

logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s %(name)s [%(levelname)-5.5s]  %(message)s",
    handlers=[
        logging.StreamHandler()
    ])
logger = logging.getLogger(__name__)

parser = argparse.ArgumentParser()
parser.add_argument("-get", "--get", help="URL to process")
parser.add_argument("-gui", "--gui", action='store_true', help="Enable GUI")
parser.add_argument("-view", "--view", help="View stored data")


def get_content_by_url(url):
    logger.debug(u'Processing URL: {}'.format(url))
    with urllib.request.urlopen(url) as conn:
        if conn.code / 100 >= 4:
            logger.error("Failed to load url: {}".format(url))
        return conn.read()


def get_html_elements(input_url):
    html_content = get_content_by_url(input_url)
    html_tree = create_html_tree(html_content)
    return Counter([element.tag for element in html_tree.iter()])


def create_html_tree(content):
    return lxml.html.fromstring(content)


def init_gui(get_html_elements_func):
    def reply():
        text.delete(1.0, END)

        url = entry.get()
        data = yaml.safe_load(
            open("synonym.yaml"))
        if url in data:
            url = data.get(url)

        elements = get_html_elements_func(url)
        save_web_data(url, elements)
        text.insert(12.0, elements)

    def func(event):
        reply()

    window = Tk()

    button = Button(window, text='Start', command=reply)
    entry = Entry(width=50)
    entry.bind('<Return>', func)
    text = Text(height=15, wrap=WORD)
    text.grid_columnconfigure(12)

    entry.pack()
    button.pack()
    text.pack()

    window.mainloop()


def save_web_data(input_url: str, elements: Counter):
    new_item = WebPageData()
    new_item.url = input_url
    new_item.host_name = urlparse(input_url).hostname
    new_item.check_date = datetime.now()

    tags_dict = dict(filter(lambda item: isinstance(item[0], str), dict(elements).items()))
    new_item.parsing_data = json.dumps(tags_dict)

    session.add(new_item)
    session.commit()


def main():
    logger.info(u'START')
    args = parser.parse_args()

    gui_flag = args.gui
    if args.view:
        input_url = args.view

        data = yaml.safe_load(
            open("synonym.yaml"))
        if input_url in data:
            input_url = data.get(input_url)
        print(json.loads(session.query(WebPageData).filter(WebPageData.url == input_url).all()[0].parsing_data))
    elif gui_flag:
        logger.info(u'GUI enabled')
        init_gui(get_html_elements)
    else:
        input_url = args.get

        data = yaml.safe_load(
            open("synonym.yaml"))
        if input_url in data:
            input_url = data.get(input_url)

        elements = get_html_elements(input_url)
        print(elements)
        save_web_data(input_url, elements)

    logger.info(u'END')


if __name__ == '__main__':
    main()
